# Minimum Qsub usage for job submission on NSCC 

## 1. Basics 

### Script 
- `-N`: name the job. e.g. `-N MBL`, name the job as MBL. **This name will be used as prefix all job output files for both standard output and errors in the home directory. The name also appears in the queue listing once submitted** 
- `-l`: define the resources the are required and establishes a limit to the amount of resource that can be used including:
    - `-l nodes=8:ppn=4` nodes number. 8 nodes each with 4 threads 
    - `-l ncpus=8` threads number used. 
    - `-l mem=32G` use 32G memory 
    - `-l walltime=24:00:00` the job shall be finished within 24 hours, if the time used exceed this limit, the resources might be released and job would not be finished. 
    - `-l select=1` allocate number of separate nodes (difference with nodes?) 
    - `-l ompthreads=1` OpenMP thread to be 1. (fail to identify and source for this)
- `-o`: `<output_logfile>` with name 
- `-e`: `<output_errorfile>` with name
- `-m a/b/e`: send you a email when job is completed or aborts.
    - `a` to send a mail when the job is aborted by the batch system.
    - `b` to send a mail when the job begins.
    - `e` to send a mail when the job is completed. 
- `-M` put in your email address. Might send to the default email is it is not defined.

### Command line 

#### `qsub` Submit a job 
`qsub` command line option:
- `qsub -q queuename script.pbs`
    For NSCC(Singapore), internal queue name 
        - `dev` Walltime: 2 hours 2 standard nodes 
        - `small` Walltime: 24 hours Up to 24 cores per job 
        - `medium` Walltime: 24 hours Up to the limit as per prevail polices 
        - `long` Walltime: 120 hours One node per user 

#### `qdel` Delete a job 
```
qdel job_id #Delete a job 
qdel all #Delete all jobs 
```

#### `qstat` Monitoring job status 
```
qstat [Job ID]  #Job status for specific job 
qstat -a  
qstat -q 
qstat -Q #Print Queue information
qstat -B #Print cluster information
qstat -x #Job history 
qstat -f [Job ID] #Job status with all information 
```

#### `showstart` Estimate starting time  (Not applicable to NSCC(SG))
```
showstart 
```

#### qfree Show free cpus ande nodes. 

## 2. Example
```qsub
#!/bin/bash 
#PBS -q normal 
#PBS -l select=1:ncpus=8:mem=32G:ompthreads=1 
#PBS -l walltime=120:00:00
#PBS -o /home/users/sutd/username/outputfiles/yourexec.o
#PBS -e /home/users/sutd/usernmae/errorfiles/yourexec.o 
module load composerxe/2017.0.098 boost/1.59.0/gcc493/serial

home/user/sutd/username/exec Parameter1:p1 Parameter2:p2 Parameter 3 p3 ...
```

## 3. Multiple job submissions
To submit multiple jobs with varying parameters, we need to use a bash script. 
- Suppose the executable command is as given in the example `home/user/sutd/username/exec Parameter1:p1 Parameter2:p2 Parameter 3 p3` with three parameters. 
- We first change the parameter to variable, i.e. `home/user/sutd/username/exec ${p1var} ${p2var} ${p3var}.`
- Use `bash` script to launch multiple `qsub` script

The bash script is as follows.
``` bash 
#Move the load comment outside to avoid repeated loading.
module load composerxe/2017.0.098 boost/1.59.0/gcc493/serial
#!/bin/bash
    for p1 in 1 2 3 4 5; do
        for p2 in {0.1,0.2,0.3,0.4,0.5}; do 
            for p3 in $(seq 1 100) do
                qsub -v p1var=$p1,p2var=$p2,p3var=$p3 qsubscript.pbs  #Note: there should be no space between the parameter list.
                sleep 1s #This line is added to prevent unsuccessful loading of modules required. However, there there will still be mysterious problem for unsuccessful load of module ocassionally.
            done 
        done 
    done 
```

### 3.1 A more complete example

The pbs script
``` bash 
#!/bin/bash
#PBS -P Personal
#PBS -q normal
#PBS -l select=1:ncpus=1:mem=1G:ompthreads=1
#PBS -l walltime=2:00:00
module load composerxe/2017.0.098 boost/1.59.0/gcc493/serial hdf5/1.8.16/gcc493/serial
/home/users/sutd/xiansong/mps-label/build/test/test_spinchain maxbonddimension:256 L:8 W:${p1} D:${p2} tf:400 order:2 dt:0.005 measurestep:20 g:0.05 R:${p3}
```

The bash script
``` bash 
for sample in $(seq 60 75);do
        for p1var in {0.001,0.1,0.5,1,3,5,7,10,15}; do
                for p2var in {0.001,0.1,0.5,1,3,5,7,10,15}; do
                        for p3var in $sample; do
                        qsub -o /home/users/sutd/xiansong/outputfiles/W${p1var}D${p2var}R${p3var}.o -e /home/users/sutd/xiansong/errorfiles/W${p1var}D${p2var}R${p3var}.e -v p1=$p1var,p2=$p2var,p3=$p3var L8submod.pbs
                        sleep 1s
                        done
                done
        done
        if [ $sample -ne 75 ]
        then
        echo "Instance $sample is submitted"
        sleep 55m #Maximum 100 jobs can be submitted at one time. Wait 55m for all the jobs to be finished. Try to find a better solution to auto detect the jobs and added more systematically. 
        fi
done
echo "All job submitted, Sample 75 collected"

```


## 4. Reference

1. [A complete list of arguments](http://docs.adaptivecomputing.com/torque/4-0-2/Content/topics/commands/qsub.htm)
2. [MDC-berlin](http://bioinformatics.mdc-berlin.de/intro2UnixandSGE/sun_grid_engine_for_beginners/how_to_submit_a_job_using_qsub.html)
3. [Gatech Pace document](https://pace.gatech.edu/job-submission-0)
4. [NSCEE Wiki](http://wiki.nscee.edu/index.php/How_to_run_jobs_with_PBS/Pro)
5. [中科院](http://hydro.igsnrr.ac.cn/data/resources/PBS.pdf)
6. [CAS Lab of Space weather](http://www.spaceweather.ac.cn/~zhhuang/advanced.pdf)
7. [Quick reference Guide for users](https://help.nscc.sg/wp-content/uploads/2016/08/PBS_Professional_Quick_Reference.pdf)
